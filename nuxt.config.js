export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,
  server: {
    port: 3005,
    host: "0.0.0.0" // default: localhost
  },
  env: {
    // API_URL: process.env.API_URL,
    // API_URL2 : process.env.API_URL2,
    API_PROXY_URL: process.env.API_PROXY_URL,
    DOMAIN: process.env.DOMAIN,
    TITLE: process.env.TITLE,
    LOGO : process.env.LOGO,
    LOGOTITLE : process.env.LOGOTITLE
  },
  // Target: https://go.nuxtjs.dev/config-target
  target: "static",

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: process.env.TITLE || 'WINBET97',
    htmlAttrs: {
      lang: "en"
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "og:url", property: "og:url", content: "WINBET97.COM" },
      { hid: "og:title", property: "og:title", content: "Welcome to WINBET97.COM" },
      { hid: "og:description", property: "og:description", content: "คาสิโนออนไลน์ระดับโลก WINBET97.COM" },
      { hid: "og:image", property: "og:image", content: "/logo.png" },
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css?family=Prompt",
      },

    ],
    script: [
      {
        src: "https://code.jquery.com/jquery-3.6.0.js",
        type: "text/javascript"
      }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/css/main.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    // { src: '~/plugins/Winwheel.js' },
    // { src: '~/plugins/TweenMax.min.js' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/google-fonts',

    // With options
    ['@nuxtjs/google-fonts', { /* module options */ }]
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    // '@nuxtjs/proxy',
    "bootstrap-vue/nuxt",
    // https://go.nuxtjs.dev/axios
    "@nuxtjs/axios",
    "vue-sweetalert2/nuxt",
    // "vue-slick"
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // proxy: true,
    baseURL: "",
    crossDomain:true,
    proxyHeaders: false,
    credentials: false,
  },
  proxy: {
    "/get.php": {
      target: "https://winbet97.com",
      pathRewrite: { "^/api/": "" }
    },
    // '/public': { target: 'https://zamba777.com',changeOrigin: true, pathRewrite: {'^/public/': '/public'} }

    



  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    analyze: {
      analyzerMode: "static"
    },
    babel: {
      compact: false,
      plugins: [
        ['@babel/plugin-proposal-private-methods', { loose: true }]
      ]
    },
    maxChunkSize: 300000
    // publicPath:"_nuxt/"
  },
  router: {
    mode: "hash",
    // base: "/betkub"
  },
  googleFonts: {
    families: {
      Kanit: true
      
    }
  }
};
